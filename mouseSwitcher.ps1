#You can look for more cursors here : http://www.rw-designer.com/gallery?search=crosshair
#Copy them in your Cursors folders : C:\Windows\Cursors\
$cursorModified = "%SystemRoot%\cursors\_crosshair.cur"
$cursorNormal = "%SystemRoot%\cursors\aero_arrow.cur"

if($args[0] -eq "invisible") {
	$cursorModified = "%SystemRoot%\cursors\_invisible.cur"
}

#Save the cursor location in the Windows registry
$RegConnect = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]"CurrentUser","$env:COMPUTERNAME")
$RegCursors = $RegConnect.OpenSubKey("Control Panel\Cursors", $true)
$boolModified = 0
#If cursor is "standard", switch to a "modified" one - and vice versa
if(((Get-ItemProperty -Path "Registry::HKEY_CURRENT_USER\Control Panel\Cursors" -Name Arrow).Arrow) -eq $cursorNormal) {
   $RegCursors.SetValue("Arrow", $cursorModified)
   $boolModified = 1
} else {
   $RegCursors.SetValue("Arrow", $cursorNormal)
}
$RegCursors.Close()
$RegConnect.Close()
#Update the registry in the current session : https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-systemparametersinfoa
function Update-CursorUserPreferencesMask {
$Signature = @"
[DllImport("user32.dll", EntryPoint = "SystemParametersInfo")]
public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, uint pvParam, uint fWinIni);
const int SPI_SETCURSORS = 0x0057;
const int SPIF_UPDATEINIFILE = 0x01;
const int SPIF_SENDCHANGE = 0x02;
public static void UpdateUserPreferencesMask() {
    SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
}
"@
    Add-Type -MemberDefinition $Signature -Name UserPreferencesMaskSPI -Namespace User32
    [User32.UserPreferencesMaskSPI]::UpdateUserPreferencesMask()
}
Update-CursorUserPreferencesMask

#If you ask for a return to normal, and the cursor has already been modified
if($args[1] -eq "prompt" -and $boolModified) {
	$msg = '
	
	
	The cursor is in "crosshair" mode.
	
	Restore the default one? [Y/N]'
	if($args[0] -eq "invisible") {
		$msg = '
		
		
	The cursor is invisible!
		
	Restore the default one? [Y/N]'
	}
	$response = Read-Host -Prompt $msg
	#go back to normal
	if ($response.ToLower() -eq 'y' -or $response.ToLower() -eq 'yes') {
		$RegConnect = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]"CurrentUser","$env:COMPUTERNAME")
		$RegCursors = $RegConnect.OpenSubKey("Control Panel\Cursors", $true)
		$RegCursors.SetValue("Arrow", $cursorNormal)
		$RegCursors.Close()
		$RegConnect.Close()
		Update-CursorUserPreferencesMask
	}
}