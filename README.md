# Lightgun - Mouse Crosshair or No Mouse Cursor Pointer

## Launch a simple .bat file to change your mouse cursor in one click!

## Have a crosshair pointer for all your lightgun games
## ...or get rid of "nomousy.exe" and hide your mouse cursor/pointer in one click!

1) Copy "_crosshair.cur" and "_invisible.cur" in your Windows system Cursors folders => C:\Windows\Cursors
2) Click "Continue" when permission is asked

3) Launch "crosshair.bat" if you want to use a crosshair cursor before starting your game
4) Press "y" and "enter" to restore your old cursor after playing your game

OR

3) Launch "invisible.bat" if you want to hide the mouse cursor before starting your game
4) Press "y" and "enter" to restore your old cursor after playing your game

--

If you prefer to restore the cursor by clicking a second time on the .bat file, use the "_noprompt.bat" scripts.

Enjoy your games!